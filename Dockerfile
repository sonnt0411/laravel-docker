FROM php:7.3-apache

RUN groupadd -g 1000 sonnt && \
    useradd -u 1000 -g sonnt sonnt && \
    apt update && \
    apt install -y \
        libzip-dev \
        git \
        unzip \
        zlib1g-dev && \
    docker-php-ext-install \
        zip \
        pdo_mysql && \
    a2enmod rewrite

# XDEBUG
RUN pecl install xdebug-2.9.8
RUN docker-php-ext-enable xdebug

COPY docker/composer-installer.php /tmp/composer-installer.php
RUN php /tmp/composer-installer.php --install-dir=/usr/local/bin --filename=composer

COPY docker/vhosts.conf /etc/httpd/conf.d/vhosts.conf

COPY docker/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

# copy source code
COPY . /var/www/html/
RUN chown -R sonnt /var/www/html/

ENV APACHE_RUN_USER sonnt

# change document root
# see: https://hub.docker.com/_/php
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# dojo-core settings
WORKDIR /var/www/html/

RUN cp .env.example .env
RUN composer install

RUN php artisan key:generate
